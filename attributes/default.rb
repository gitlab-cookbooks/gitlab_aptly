default['aptly']['secrets']['backend'] = 'chef_vault'
default['aptly']['secrets']['path'] = 'aptly'
default['aptly']['secrets']['key'] = 'prd'
default['aptly']['tls']['ssl_key']['path'] =
  '/etc/ssl/private/aptly.gitlab.com.key'
default['aptly']['tls']['ssl_key']['content'] = ''
default['aptly']['tls']['ssl_cert']['path'] =
  '/etc/ssl/certs/aptly.gitlab.com.crt'
default['aptly']['tls']['ssl_cert']['content'] = ''
default['aptly']['tls']['ssl_cert']['path'] =
  '/etc/ssl/certs/aptly.gitlab.com.crt'
default['aptly']['tls']['ssl_cert']['path'] =
  '/etc/ssl/certs/aptly.gitlab.com.crt'
default['aptly']['basic_auth']['user'] = ''
default['aptly']['basic_auth']['password'] = ''
default['aptly']['basic_auth']['path'] = '/etc/nginx/.htpasswd.aptly'
# this one seem to be faster than default one
default['aptly']['keyserver'] = 'pgp.mit.edu'
