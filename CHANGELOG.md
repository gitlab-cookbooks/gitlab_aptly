# CHANGELOG.md

### v0.0.2 (Jun 2017) -- Ilya Frolov (ilya@gitlab.com)
 * Made great again

### v0.0.1 (Dec 2016)
 * Original version of the gitlab_aptly cookbook -- [Jeroen Nijhof](jeroen@jeroennijhof.nl)
