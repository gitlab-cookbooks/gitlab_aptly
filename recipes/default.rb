# Cookbook Name:: gitlab_aptly
# Recipe:: default
# License:: MIT
#
# Copyright 2016, GitLab Inc.

include_recipe 'aptly'
include_recipe 'gitlab_aptly::gpg'
include_recipe 'gitlab_aptly::frontend'

template '/etc/systemd/system/aptly.service' do
  source 'aptly.service.erb'
  mode '0440'
  owner 'root'
  group 'root'
  notifies :run, 'execute[systemctl-enable-aptly]', :delayed
end

execute 'systemctl-enable-aptly' do
  command 'systemctl enable aptly && systemctl start aptly'
  action :nothing
end
