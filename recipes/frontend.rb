# Cookbook Name:: gitlab_aptly
# Recipe:: frontend
# License:: MIT
# Frontend for aptly directory and /api/ endpoint, currently nginx
#
# Copyright 2016, GitLab Inc.

package('nginx') do
  action :install
end

# Get secrets if necessary
if node['aptly']['tls']['ssl_key']['content'].empty? ||
   node['aptly']['tls']['ssl_cert']['content'].empty? ||
   node['aptly']['basic_auth']['user'].empty? ||
   node['aptly']['basic_auth']['password'].empty?
  secrets_hash = node['aptly']['secrets']
  secrets = get_secrets(
    secrets_hash['backend'],
    secrets_hash['path'],
    secrets_hash['key']
  )

  node.default['aptly']['tls']['ssl_key']['content'] = secrets['ssl_key']
  node.default['aptly']['tls']['ssl_cert']['content'] = secrets['ssl_cert']
  node.default['aptly']['basic_auth']['user'] = secrets['basic_auth_user']
  node.default['aptly']['basic_auth']['password'] =
    secrets['baisc_auth_password']
end

file node['aptly']['tls']['ssl_key']['path'] do
  content node['aptly']['tls']['ssl_key']['content']
  mode '0377'
  owner 'root'
  group 'root'
  notifies :run, 'execute[nginx_test_and_restart]', :immediately
end

file node['aptly']['tls']['ssl_cert']['path'] do
  content node['aptly']['tls']['ssl_cert']['content']
  mode '0377'
  owner 'root'
  group 'root'
  notifies :run, 'execute[nginx_test_and_restart]', :immediately
end

template node['aptly']['basic_auth']['path'] do
  source '.htpasswd.aptly.erb'
  mode '0444'
  owner 'root'
  group 'root'
end

template '/etc/nginx/sites-available/aptly.conf' do
  source 'aptly.conf.erb'
  mode '0440'
  owner 'root'
  group 'root'
  notifies :run, 'execute[apply-nginx-config]', :immediately
  notifies :run, 'execute[nginx_test_and_restart]', :immediately
end

execute 'apply-nginx-config' do
  command <<-NGINX
    rm -f /etc/nginx/sites-enabled/default && \
    ln -s /etc/nginx/sites-available/aptly.conf /etc/nginx/sites-enabled/aptly.conf
  NGINX
  action :nothing
end

execute 'nginx_test_and_restart' do
  command '/usr/sbin/nginx -t'
  returns 0
  action :nothing
  notifies :restart, 'service[nginx]', :delayed
end

service 'nginx' do
  action :enable
end
