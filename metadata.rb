name             'gitlab_aptly'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact+cookbooks@gitlab.com'
license          'MIT'
description      'Cookbook gitlab_aptly for GitLab cookbooks'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.5'
chef_version     '>= 12.1' if respond_to?(:chef_version)
issues_url       'https://gitlab.com/gitlab-cookbooks/gitlab_aptly/issues'
source_url       'https://gitlab.com/gitlab-cookbooks/gitlab_aptly'

supports 'ubuntu', '= 16.04'

# Please specify dependencies with version pin:
depends 'aptly', '~> 1.1.0'
# netiher nginx not chef_nginx can be enabled here.
# therefore we install nginx directly and supply it with config from
# template.
# depends 'chef_nginx', '~> 2.7.0'
depends 'gitlab_secrets'
