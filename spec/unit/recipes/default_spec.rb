# Cookbook:: gitlab_aptly
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.

require 'spec_helper'

describe 'gitlab_aptly::default' do
  context 'when all attributes are default, on Ubuntu 16.04' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04')
                            .converge(described_recipe)
    end

    # aptly cookbook does not converge on 16.04.
    # no unit tests for now, see integration ones
  end
end
