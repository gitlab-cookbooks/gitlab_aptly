# InSpec tests for recipe gitlab_aptly::default

control 'general-aptly-package' do
  impact 1.0
  title 'General checks for aptly package'
  desc '
    This control ensures that:
      * aptly package is installed
      * aptly package version is 1.4.0
      * creates non-root, system aptly user with home /opt/aptly
  '

  describe package('aptly') do
    it { should be_installed }
    its('version') { should eq '1.4.0' }
  end

  describe user('aptly') do
    it { should exist }
    its('home') { should eq '/opt/aptly' }
    its('uid') { should be > 0 }
    its('uid') { should be < 1000 } # assuming UID_MIN is 1000
  end
end

control 'general-aptly-service' do
  impact 1.0
  title 'General checks for aptly service'
  desc '
    This control ensures that:
      * aptly.service systemd file is present
      * aptly service is installed
      * aptly service is enabled
      * aptly service is running
  '

  describe file('/etc/systemd/system/aptly.service') do
    it { should be_file }
    its('type') { should eq :file }
    its('owner') { should eq 'root' }
    its('group') { should eq 'root' }
    its('mode') { should cmp '0440' }
  end

  describe service('aptly') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end

control 'general-aptly-process' do
  impact 1.0
  title 'General checks for aptly service'
  desc '
    This control ensures that:
      * aptly process is running as aptly user
      * aptly process is bound to tcp://127.0.0.1:8000
  '
  describe processes('aptly') do
    it { should exist }
    its('users') { should eq ['aptly'] }
    its('commands') { should eq ['/usr/bin/aptly api serve -listen 127.0.0.1:8000 -no-lock'] } # rubocop:disable LineLength
  end

  describe port('8000') do
    it { should be_listening }
    its('addresses') { should eq ['127.0.0.1'] }
    its('protocols') { should eq ['tcp'] }
    its('processes') { should eq ['aptly'] }
  end

  # it would be cleaner to use http resource for that check, but
  # it only runs on host where kitchen tests are run:
  # https://www.inspec.io/docs/reference/resources/http/
  # therefore, good old bash until that is changed
  describe bash('wget -O - -q http://127.0.0.1:8000/api/version') do
    its('exit_status') { should eq 0 }
    its('stdout.strip') { should eq '{"Version":"1.4.0"}' }
    its('stderr') { should eq '' }
  end
end

control 'general-aptly-gpg' do # rubocop:disable BlockLength
  impact 1.0
  title 'GPG checks for aptly'
  desc '
    This control ensures that:
      * entropy filler haveged is installed and running
      * GPG keys for aptly are generated
      * apt-key can add gpg public key
  '

  describe package('haveged') do
    it { should be_installed }
  end

  describe service('haveged') do
    it { should be_enabled }
    it { should be_running }
  end

  describe file('/opt/aptly/.gnupg/secring.gpg') do
    it { should be_file }
    its('type') { should eq :file }
    its('owner') { should eq 'aptly' }
    its('group') { should eq 'aptly' }
    its('mode') { should cmp '0600' }
    # assume non zero file size means key was generated
    # instead of testing gpg --list-secret-keys output
    its('size') { should be > 0 }
  end

  describe bash('apt-key add /opt/aptly/.gnupg/pubring.gpg') do
    its('exit_status') { should eq 0 }
    its('stdout.strip') { should eq 'OK' }
    its('stderr') { should eq '' }
  end
end

control 'general-aptly-frontend' do # rubocop:disable BlockLength
  impact 1.0
  title 'Frontend checks for aptly service'
  desc '
    This control ensures that:
     * nginx is installed
     * nginx is running
     * nginx is listening on 0:80 and 0:443
  '
  describe package('nginx') do
    it { should be_installed }
  end

  describe service('nginx') do
    it { should be_enabled }
    it { should be_running }
  end

  describe port('80') do
    it { should be_listening }
    its('addresses') { should eq ['0.0.0.0'] }
    its('protocols') { should eq ['tcp'] }
    its('processes.first') { should match(/nginx/) }
  end

  describe port('443') do
    it { should be_listening }
    its('addresses') { should eq ['0.0.0.0'] }
    its('protocols') { should eq ['tcp'] }
    its('processes.first') { should match(/nginx/) }
  end

  # same as above about http resource
  # what it does:
  #   * defines localhost as aptly.gitlab.com (for cert verification)
  #   * checks frontend api access with ci user credentials
  #     and treating selfsigned key as trusted CA
  # NOTE: if you remove '-q', then all tests pass, but inspec fails with
  # 'incompatible character encodings: UTF-8 and ASCII-8BIT'. Go figure
  describe bash('echo "127.0.0.1 aptly.gitlab.com" >> /etc/hosts && wget -q -O - --ca-certificate=/etc/ssl/certs/aptly.gitlab.com.crt --user=ci_api_user --password=disposable_ci_password_insecure_af https://aptly.gitlab.com:443/api/version') do # rubocop:disable LineLength
    its('exit_status') { should eq 0 }
    its('stdout.strip') { should eq '{"Version":"1.4.0"}' }
    its('stderr') { should eq '' }
  end
end

control 'general-aptly-api' do # rubocop:disable BlockLength
  impact 1.0
  title 'API checks for aptly service via external https'
  desc '
    This control ensures that:
      * file can be uploaded to aptly via api
      * new local repo can be created via api
      * package can be added to local
      * repo can be published
  '
  CURL = 'curl --silent --show-error --user ci_api_user:disposable_ci_password_insecure_af --cacert /etc/ssl/certs/aptly.gitlab.com.crt'.freeze # rubocop:disable LineLength
  API = 'https://aptly.gitlab.com/api'.freeze
  CT = 'Content-Type: application/json'.freeze

  # file upload
  describe bash("#{CURL} -F file=@/var/cache/apt/archives/aptly_1.4.0_amd64.deb #{API}/files/aptly") do # rubocop:disable LineLength
    its('exit_status') { should eq 0 }
    its('stdout.strip') { should eq '["aptly/aptly_1.4.0_amd64.deb"]' }
    its('stderr') { should eq '' }
  end

  # new repo creation
  data = '{"Name":"ci-test-repo","Comment":"ci-test","DefaultDistribution":"xenial","DefaultComponent":"main"}' # rubocop:disable LineLength
  describe bash("#{CURL} -X POST -H '#{CT}' --data '#{data}' #{API}/repos") do
    its('exit_status') { should eq 0 }
    its('stdout.strip') { should eq data }
    its('stderr') { should eq '' }
  end

  # add uploaded file to new repo
  result = '{"FailedFiles":[],"Report":{"Warnings":[],"Added":["aptly_1.4.0_amd64 added"],"Removed":[]}}' # rubocop:disable LineLength
  describe bash("#{CURL} -X POST #{API}/repos/ci-test-repo/file/aptly") do
    its('exit_status') { should eq 0 }
    its('stdout.strip') { should eq result }
    its('stderr') { should eq '' }
  end

  # query repo for packages
  describe bash("#{CURL} #{API}/repos/ci-test-repo/packages") do
    its('exit_status') { should eq 0 }
    its('stdout.strip') { should eq '["Pamd64 aptly 1.4.0 a226665b7c8a86f"]' }
    its('stderr') { should eq '' }
  end

  # sign and publish
  # Default passphrase from: https://github.com/sous-chefs/aptly/blob/master/README.md#gpg-attributes
  data_s = '{"SourceKind":"local","Sources":[{"Name":"ci-test-repo"}],"Distribution":"xenial","Signing":{"Batch":true,"Passphrase":"GreatPassPhrase"}}' # rubocop:disable LineLength
  result_s = '{"AcquireByHash":false,"Architectures":["amd64"],"ButAutomaticUpgrades":"","Distribution":"xenial","Label":"","NotAutomatic":"","Origin":"","Prefix":"ci","SkipContents":false,"SourceKind":"local","Sources":[{"Component":"main","Name":"ci-test-repo"}],"Storage":""}' # rubocop:disable LineLength
  describe bash("#{CURL} -X POST -H '#{CT}' --data '#{data_s}' #{API}/publish/ci") do # rubocop:disable LineLength
    its('exit_status') { should eq 0 }
    its('stdout.strip') { should eq result_s }
    its('stderr') { should eq '' }
  end

  # add repo and do apt-get update
  describe bash('echo "deb http://aptly.gitlab.com/ci xenial main" >> /etc/apt/sources.list && apt-get -qq update') do # rubocop:disable LineLength
    its('exit_status') { should eq 0 }
    its('stdout') { should eq '' }
    its('stderr') { should eq '' }
  end

  # make sure we actially overwrite package when uploading
  # this is critical for CI
  # for that, just build an empty package with same name/version/arch
  # and try to upload it
  describe bash('cd /tmp/; export V="1.4.0"; mkdir -p aptly_${V}/DEBIAN; echo -e "Package: aptly\nVersion: ${V}\nMaintainer: cibot\nArchitecture: amd64\nDescription: gitlab-aptly mock package\n" > aptly_${V}/DEBIAN/control; dpkg-deb --build aptly_${V} aptly_${V}_amd64.deb >/dev/null') do # rubocop:disable LineLength
    its('exit_status') { should eq 0 }
    its('stdout') { should eq '' }
    its('stderr') { should eq '' }
  end
  # file upload
  describe bash("#{CURL} -F file=@/tmp/aptly_1.4.0_amd64.deb #{API}/files/aptly") do # rubocop:disable LineLength
    its('exit_status') { should eq 0 }
    its('stdout.strip') { should eq '["aptly/aptly_1.4.0_amd64.deb"]' }
    its('stderr') { should eq '' }
  end
  # add uploaded file to new repo
  result_a = '{"FailedFiles":[],"Report":{"Warnings":[],"Added":["aptly_1.4.0_amd64 added"],"Removed":["aptly_1.4.0_amd64 removed due to conflict with package being added"]}}' # rubocop:disable LineLength
  describe bash("#{CURL} -X POST #{API}/repos/ci-test-repo/file/aptly?forceReplace=1") do # rubocop:disable LineLength
    its('exit_status') { should eq 0 }
    its('stdout.strip') { should eq result_a }
    its('stderr') { should eq '' }
  end
  # sign and publish again
  data_sn = '{"ForceOverwrite":true,"Signing":{"Batch":true,"Passphrase":"GreatPassPhrase"}}' # rubocop:disable LineLength
  result_sn = '{"AcquireByHash":false,"Architectures":["amd64"],"ButAutomaticUpgrades":"","Distribution":"xenial","Label":"","NotAutomatic":"","Origin":"","Prefix":"ci","SkipContents":false,"SourceKind":"local","Sources":[{"Component":"main","Name":"ci-test-repo"}],"Storage":""}' # rubocop:disable LineLength
  describe bash("#{CURL} -X PUT -H '#{CT}' --data '#{data_sn}' #{API}/publish/ci/xenial") do # rubocop:disable LineLength
    its('exit_status') { should eq 0 }
    its('stdout.strip') { should eq result_sn }
    its('stderr') { should eq '' }
  end
  # download and compare
  describe bash('diff {/opt/aptly/public/ci/pool/main/a/aptly,/tmp}/aptly_1.4.0_amd64.deb') do # rubocop:disable LineLength
    its('exit_status') { should eq 0 }
    its('stdout.strip') { should eq '' }
    its('stderr') { should eq '' }
  end
end
